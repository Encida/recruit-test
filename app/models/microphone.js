// Defines the table design of table `Microphones` in database.sqlite
module.exports = (sequelize, DataTypes) => {
  const microphones = sequelize.define('microphones',
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
      }
    },
    {
      tableName: 'microphones',
      freezeTableName: true,
      createdAt: false,
      updatedAt: false
    });
  return microphones;
};
