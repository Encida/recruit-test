const express = require('express');
const connection = require('../setup/database');

const MicrophoneModel = connection.import('../models/microphone');
const router = express.Router();

// GET: Finds an entry in the Microphones table where id = req.params.id
router.get('/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  return MicrophoneModel.findOne({ where: { id } })
    .then((microphone) => res.status(200).send(microphone))
    .catch((error) => res.status(500).send({ error }));
});

// POST: Creates a new entry in Microphones
router.post('/', (req, res) => {
  const { body } = req;
  return res.status(200).send(body);
});

module.exports = router;
