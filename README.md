## Prerequisites

You will need the following installed on your computer.

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/download/releases/) v. 8.12.0 (with NPM)

## Installation

`git clone https://gitlab.com/Encida/recruit-test.git`  
`cd recruit-test`  
`npm install`

## Running / Development

`npm start`  
Use Postman or similar to test endpoints at http://localhost:5000/
