const express = require('express');
const registerErrorHandler = require('./error-handler');
const microphoneController = require('./controllers/microphone');

// Set up the express app
const app = express();

// Parse incoming requests data.
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Setup the micrphones route and error handlers
app.use('/microphones', microphoneController);
registerErrorHandler(app);

// Start the server on localhost:5000
app.listen(5000, () => {
  console.log('Server listening on port 5000.');
});
